# HelloWorld Node.js and React app (coder-react)

###### Instructions

1. Open the Coder button to create a workspace in a specific Coder deployment (Kubernetes cluster where Coder is installed)
1. Choose either a template button or an image button to build a workspace based on the image, compute and configuration settings


<details>
  <summary>More Information & Coder Documentation</summary>

[Workspace templates](https://coder.com/docs/coder/latest/workspaces/workspace-templates) are declarative YAML files that describe
how to configure Coder workspaces and their supporting infrastructure. Coder supports
files with either the `.yaml` or `.yml` extension.

[Images](https://coder.com/docs/coder/latest/workspaces/images) contain the IDEs, CLIs, language versions, and dependencies users need to work on software development projects. Users can create workspaces with the image as the blueprint, then begin contributing immediately to the projects for which the image was defined. Images may contain [configure scripts](https://coder.com/docs/coder/latest/images/configure) that Coder runs after the workspace build to add additional dependencies, VS Code extensions, clone repositories, or run CLI commands.

> Both templates and images require that OAuth has been configured on the GitLab deployment and the Coder user has linked their Coder account to GitLab in their Coder Account Profile for git clone, push and pull actions.
</details>

> A Coder workspace is an isolated pod with an inner container managed by the Coder control plane running in a namespace of a Kubernetes cluster. The Coder deployments below are running on Google Cloud GKE but could be any Kubernetes provider. e.g., Rancher, OpenShift, AWS EKS, Azure AKS, etc.

  
##### from image
  
###### clean.demo.coder.com
[![Open in Coder](https://cdn.coder.com/embed-button.svg)](https://clean.demo.coder.com/workspaces/git?org=default&image=613e7962-fe3f5efcfd8ce7cb502825b6&tag=ubuntu&service=gitlab&repo=git@gitlab.com:mtm20176/coder-react.git)


##### from template

###### clean.demo.coder.com
[![Open in Coder](https://clean.demo.coder.com/static/image/embed-button.svg)](https://clean.demo.coder.com/wac/build?template_oauth_service=625ff6b7-9e0fbb71f34a2ed66ae5a2e5&template_url=git@gitlab.com:mtm20176/coder-react.git&template_ref=main&template_filepath=.coder/coder.yaml)














